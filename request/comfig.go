package request

import (
	"net/http"
	"net/url"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/keypair/figurekeypair"

	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/signed"
)

type Reviewerer interface {
	Reviewer() *Reviewer
}

type reviewerer struct {
	getter kv.Getter
	once   comfig.Once
	keyer.Keyer
}

func NewReviewerer(getter kv.Getter) Reviewerer {
	return &reviewerer{
		getter: getter,
		Keyer:  keyer.NewKeyer(getter),
	}
}

func (r *reviewerer) Reviewer() *Reviewer {
	return r.once.Do(func() interface{} {
		keys := r.Keyer.Keys()

		config := struct {
			Endpoint      *url.URL `fig:"endpoint,required"`
			WaitResult    bool     `fig:"wait_result"`
			WaitForIngest bool     `fig:"wait_for_ingest"`
		}{
			WaitResult: true,
		}

		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(r.getter, "reviewer")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out reviewer"))
		}

		cli := signed.NewClient(http.DefaultClient, config.Endpoint)
		if keys.Signer != nil {
			cli = cli.WithSigner(keys.Signer)
		}

		return New(cli, keys, config.WaitResult, config.WaitForIngest)
	}).(*Reviewer)
}
