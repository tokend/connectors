package rbac

import (
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type Connectorer interface {
	Connect() *Connector
}

type connectorer struct {
	getter kv.Getter
	once   comfig.Once

	keyer.Keyer
}

func NewConnectorer(getter kv.Getter) *connectorer {
	return &connectorer{
		getter: getter,
		Keyer:  keyer.NewKeyer(getter),
	}
}

func (h *connectorer) Connect() *Connector {
	return h.once.Do(func() interface{} {
		keys := h.Keyer.Keys()
		var config struct {
			Endpoint  *url.URL `fig:"endpoint,required"`
			SkipCheck bool     `fig:"skip_check"`
		}

		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(h.getter, "rbac")).
			Please()
		if err != nil {
			fmt.Println(errors.GetFields(err))
			panic(errors.Wrap(err, "failed to figure out rbac"))
		}

		cli := signed.NewClient(http.DefaultClient, config.Endpoint)
		if keys.Signer != nil {
			cli = cli.WithSigner(keys.Signer)
		}

		return New(cli, config.SkipCheck)
	}).(*Connector)
}
