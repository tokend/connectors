package rbac

import (
	"context"
	"net/http"
	"net/url"

	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/json-api-connector/cerrors"
	"gitlab.com/distributed_lab/json-api-connector/client"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/rbac-svc/resources"
)

type Connector struct {
	base            *connector.Connector
	policyUrl       *url.URL
	verificationUrl *url.URL
	skipCheck       bool
}

func New(client client.Client, skipCheck bool) *Connector {
	policyUrl, _ := url.Parse("/policies")
	verificationUrl, _ := url.Parse("/verifications")

	return &Connector{
		base:            connector.NewConnector(client),
		policyUrl:       policyUrl,
		verificationUrl: verificationUrl,
		skipCheck:       skipCheck,
	}
}

func (t *Connector) CreatePolicy(subject, object, action string, isAllowed bool) error {
	request := resources.PolicyResponse{
		Data: resources.Policy{
			Attributes: resources.PolicyAttributes{
				Action:    action,
				Subject:   subject,
				Object:    object,
				IsAllowed: isAllowed,
			},
		},
	}

	err := t.base.PostJSON(t.policyUrl, request, context.TODO(), nil)
	if err != nil {
		return errors.Wrap(err, "failed to post policy")
	}

	return nil
}

func (t *Connector) CheckPermission(subject, object, action string) (bool, error) {
	if t.skipCheck {
		return true, nil
	}

	request := resources.ConditionResponse{
		Data: resources.Condition{
			Attributes: resources.ConditionAttributes{
				Action:  action,
				Subject: subject,
				Object:  object,
			},
		},
	}

	err := t.base.PostJSON(t.policyUrl, request, context.TODO(), nil)
	if err != nil {
		if cErr, ok := err.(cerrors.Error); ok {
			if cErr.Status() == http.StatusForbidden {
				return false, nil
			}
		}

		return false, errors.Wrap(err, "failed to post policy")
	}

	return true, nil
}
