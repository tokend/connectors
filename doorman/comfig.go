package doorman

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/doorman-svc/doorman"
	"net/url"
)

type Config struct {
	Addr *url.URL `fig:"addr"`
}

type Doormaner interface {
	Doorman() *doorman.Client
}

type doormaner struct {
	getter kv.Getter
	once   comfig.Once
}

func NewDoormaner(getter kv.Getter) Doormaner {
	return &doormaner{
		getter: getter,
	}
}

func (c *doormaner) Doorman() *doorman.Client {
	return c.once.Do(func() interface{} {
		var config struct {
			Addr *url.URL `fig:"addr"`
		}

		if err := figure.Out(&config).From(kv.MustGetStringMap(c.getter, "doorman")).Please(); err != nil {
			panic(errors.Wrap(err, "failed to figure out doorman"))
		}

		return doorman.NewDoormanClient(config.Addr)
	}).(*doorman.Client)
}
