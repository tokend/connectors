package signed

import (
	"errors"
	"testing"

	"github.com/spf13/cast"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tokend/keypair"
)

type TestGetter map[string]interface{}

func (t TestGetter) GetStringMap(key string) (map[string]interface{}, error) {
	entry, ok := t[key]
	if !ok {
		return nil, errors.New("No entry by key")
	}

	m, err := cast.ToStringMapE(entry)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func TestClienter_Client(t *testing.T) {
	adminPK := "GBA4EX43M25UPV4WIE6RRMQOFTWXZZRIPFAI5VPY6Z2ZVVXVWZ6NEOOB"
	adminSK := "SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4"

	servicePK := "GBW37STVLXN7CS5JE7H2UZNWDSMNIL7FFCHIRUBNQKFS53WKY4EQIN47"
	serviceSK := "SCQHCFCQHK43EKH2FQAY4UK6ZNUTXCJRIEZDSXZHRM6BH3FOIOBTLV57"
	cl := &clienter{
		getter: TestGetter{
			"keys": map[string]interface{}{
				"source": servicePK,
				"signer": serviceSK,
			},

			"client": map[string]interface{}{
				"endpoint": "api.test.tk",
				"source":   adminPK,
				"signer":   adminSK,
			},
		},
	}

	client := cl.Client()
	assert.Equal(t, client.rc.signer, keypair.MustParseSeed(adminSK))
}
