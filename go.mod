module gitlab.com/tokend/connectors

go 1.17

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cast v1.4.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/distributed_lab/figure v2.1.0+incompatible
	gitlab.com/distributed_lab/json-api-connector v0.2.4
	gitlab.com/distributed_lab/kit v1.8.6
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	gitlab.com/distributed_lab/running v1.6.0
	gitlab.com/tokend/doorman-svc v0.1.4
	gitlab.com/tokend/go v3.15.0+incompatible
	gitlab.com/tokend/keypair v0.0.0-20190412110653-b9d7e0c8b312
	gitlab.com/tokend/rbac-svc v0.0.0-20200327131412-78f659719f5d
	gitlab.com/tokend/regources v4.9.2-0.20210809204843-6ec3aa755883+incompatible
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/getsentry/sentry-go v0.7.0 // indirect
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/nullstyle/go-xdr v0.0.0-20180726165426-f4c839f75077 // indirect
	github.com/pelletier/go-toml v1.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/afero v1.1.2 // indirect
	github.com/spf13/jwalterweatherman v1.0.0 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	github.com/subosito/gotenv v1.2.0 // indirect
	github.com/xr9kayu/logrus v0.7.2 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/ini.v1 v1.51.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
