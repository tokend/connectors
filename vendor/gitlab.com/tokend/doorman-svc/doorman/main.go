package doorman

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/doorman-svc/resources"
)

const (
	headerRequestTarget     = "Request-Target"
	headerRealRequestTarget = "Real-Request-Target"
	headerSignature         = "Signature"
	headerAuthorization     = "Authorization"
	headerAccountID         = "Account-Id"
	headerDate              = "date"
)

var (
	ErrInvalidAuth  = errors.New("failed to authenticate request due to invalid authentication info")
	ErrUnauthorized = errors.New("requester is unauthorized")
)

type Client struct {
	addr *url.URL
}

// addr should be hostname (e.g. http://traefik)
func NewDoormanClient(addr *url.URL) *Client {
	return &Client{
		addr: addr,
	}
}

//  TODO try it with proxy
//  AuthenticateRequester uses doorman-svc's authentication endpoint to check signature on the request
//  returns user model of signer's account or error in case when something went wrong
func (d *Client) AuthenticateRequester(r *http.Request) (*resources.User, error) {
	req, err := createAuthenticationRequest(r, d.addr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create authentication request out of incoming request")
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to do request")
	}

	// TODO return|render errors with reasons
	switch resp.StatusCode {
	case http.StatusBadRequest:
		return nil, ErrInvalidAuth
	case http.StatusUnauthorized:
		return nil, ErrUnauthorized
	case http.StatusInternalServerError:
		return nil, errors.New("internal server error occurred while authentication")
	}

	var userResponse resources.UserResponse
	if err := json.NewDecoder(resp.Body).Decode(&userResponse); err != nil {
		return nil, errors.Wrap(err, "failed to decode user response")
	}

	return &userResponse.Data, nil
}

func createAuthenticationRequest(r *http.Request, endpoint *url.URL) (*http.Request, error) {
	drmUrl, err := endpoint.Parse("/integrations/doorman/authenticate")
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse doorman service url")
	}

	rBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read the original body")
	}

	// populate original request's body back to allow using it in handler
	r.Body = ioutil.NopCloser(bytes.NewReader(rBody))

	req, err := http.NewRequest(r.Method, drmUrl.String(), bytes.NewReader(rBody))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create authentication request")
	}

	req.Header.Set(headerRequestTarget, figureOutRequestTarget(r))
	req.Header.Set(headerRealRequestTarget, figureOutRequestTarget(r)) // DEPRECATED

	sigHeader, sigHeaderOk := safeGetHeader(r, headerSignature)
	if sigHeaderOk {
		req.Header.Set(headerSignature, sigHeader)
	}
	authHeader, authHeaderOk := safeGetHeader(r, headerAuthorization)
	if authHeaderOk {
		req.Header.Set(headerAuthorization, authHeader)
	}
	dateHeader, dateHeaderOk := safeGetHeader(r, headerDate)
	if dateHeaderOk {
		req.Header.Set(headerDate, dateHeader)
	}
	accountIDHeader, accountIDHeaderOk := safeGetHeader(r, headerAccountID)
	if accountIDHeaderOk {
		req.Header.Set(headerAccountID, accountIDHeader)
	}

	return req, nil
}

// trying ot get request-target line from request.
// priority of checks (1 == highest):
//  1. Request-Target header
//  2. Real-Request-Target header
//  3. `{method} {requestURI}` string
// Doorman internals do check request target with the same priority
func figureOutRequestTarget(r *http.Request) string {
	reqTargetHeader, reqTargetHeaderOk := safeGetHeader(r, headerRequestTarget)
	if reqTargetHeaderOk {
		return reqTargetHeader
	}
	realReqTargetHeader, realReqTargetHeaderOk := safeGetHeader(r, headerRealRequestTarget)
	if realReqTargetHeaderOk {
		return realReqTargetHeader
	}
	return r.URL.RequestURI() // fmt.Sprintf("%s %s", strings.ToLower(r.Method), r.URL.RequestURI())
}

func safeGetHeader(r *http.Request, headerName string) (headerValue string, ok bool) {
	headerValue = r.Header.Get(headerName)
	ok = headerValue != ""
	return
}
