package doorman

import (
	"fmt"

	"github.com/spf13/cast"
	"gitlab.com/tokend/doorman-svc/resources"
)

type AllowedRolesConstraint struct {
	allowedRoleIDs []uint32
}

func NewAllowedRolesConstraint(roles ...uint32) *AllowedRolesConstraint {
	return &AllowedRolesConstraint{
		allowedRoleIDs: roles,
	}
}

func (c *AllowedRolesConstraint) Description() string {
	return fmt.Sprintf("allowed role IDs are %v", c.allowedRoleIDs)
}

func (c *AllowedRolesConstraint) Check(u *resources.User) bool {
	for _, allowedRole := range c.allowedRoleIDs {
		if cast.ToUint32(u.Relationships.Role.Data.ID) == allowedRole {
			return true
		}
	}
	return false
}

type RestrictedSignerRolesConstraint struct {
	restrictedRoleIDs []uint32
}

func NewRestrictedSignerRolesConstraint(roles ...uint32) *RestrictedSignerRolesConstraint {
	return &RestrictedSignerRolesConstraint{
		restrictedRoleIDs: roles,
	}
}

func (c *RestrictedSignerRolesConstraint) Description() string {
	return fmt.Sprintf("restricted role IDs are %v", c.restrictedRoleIDs)
}

func (c *RestrictedSignerRolesConstraint) Check(u *resources.User) bool {
	for _, restrictedRole := range c.restrictedRoleIDs {
		if cast.ToUint32(u.Relationships.SignerRole.Data.ID) == restrictedRole {
			return false
		}
	}
	return true
}
