/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type UserRelationships struct {
	Role       Relation `json:"role"`
	SignerRole Relation `json:"signer_role"`
}
