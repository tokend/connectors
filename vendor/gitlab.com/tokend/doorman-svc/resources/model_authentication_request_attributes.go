/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type AuthenticationRequestAttributes struct {
	Header AuthenticationRequestHeader `json:"header"`
	Method string                      `json:"method"`
	Url    string                      `json:"url"`
}
