/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type AuthenticationRequestHeader struct {
	AccountId string `json:"account_id"`
	Signature string `json:"signature"`
}
