package resources

func MakeUserKey(userID string) Key {
	return Key{
		ID:   userID,
		Type: USERS,
	}
}

func MakeAccountRoleKey(accountRoleID string) Key {
	return Key{
		ID:   accountRoleID,
		Type: ACCOUNT_ROLES,
	}
}

func MakeSignerRoleKey(signerRoleID string) Key {
	return Key{
		ID:   signerRoleID,
		Type: SIGNER_ROLES,
	}
}
