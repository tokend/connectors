/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type AuthenticationRequest struct {
	Key
	Attributes AuthenticationRequestAttributes `json:"attributes"`
}
type AuthenticationRequestRequest struct {
	Data     AuthenticationRequest `json:"data"`
	Included Included              `json:"included"`
}

type AuthenticationRequestListRequest struct {
	Data     []AuthenticationRequest `json:"data"`
	Included Included                `json:"included"`
	Links    *Links                  `json:"links"`
	Meta     json.RawMessage         `json:"meta,omitempty"`
}

func (r *AuthenticationRequestListRequest) PutMeta(v interface{}) (err error) {
	r.Meta, err = json.Marshal(v)
	return err
}

func (r *AuthenticationRequestListRequest) GetMeta(out interface{}) error {
	return json.Unmarshal(r.Meta, out)
}

// MustAuthenticationRequest - returns AuthenticationRequest from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustAuthenticationRequest(key Key) *AuthenticationRequest {
	var authenticationRequest AuthenticationRequest
	if c.tryFindEntry(key, &authenticationRequest) {
		return &authenticationRequest
	}
	return nil
}
