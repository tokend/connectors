/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ConditionAttributes struct {
	// requested action on an object
	Action string `json:"action"`
	// some resource, can be present in other service
	Object string `json:"object"`
	// identifier of subject, whose policies will be checked
	Subject string `json:"subject"`
	Owner string `json:"owner"`
}
