/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type Policy struct {
	Key
	Attributes PolicyAttributes `json:"attributes"`
}
type PolicyResponse struct {
	Data     Policy   `json:"data"`
	Included Included `json:"included"`
}

type PolicyListResponse struct {
	Data     []Policy        `json:"data"`
	Included Included        `json:"included"`
	Links    *Links          `json:"links"`
	Meta     json.RawMessage `json:"meta,omitempty"`
}

func (r *PolicyListResponse) PutMeta(v interface{}) (err error) {
	r.Meta, err = json.Marshal(v)
	return err
}

func (r *PolicyListResponse) GetMeta(out interface{}) error {
	return json.Unmarshal(r.Meta, out)
}

// MustPolicy - returns Policy from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustPolicy(key Key) *Policy {
	var policy Policy
	if c.tryFindEntry(key, &policy) {
		return &policy
	}
	return nil
}
