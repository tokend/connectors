/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type PolicyAttributes struct {
	// An action to which policy attached
	Action string `json:"action"`
	// Defines whether policy allows or denies perform action with object for subject
	IsAllowed bool `json:"is_allowed"`
	// some resource, can be present in other service
	Object string `json:"object"`
	// identifier of subject (can be role)
	Subject string `json:"subject"`
}
