/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type Condition struct {
	Key
	Attributes ConditionAttributes `json:"attributes"`
}
type ConditionResponse struct {
	Data     Condition `json:"data"`
	Included Included  `json:"included"`
}

type ConditionListResponse struct {
	Data     []Condition     `json:"data"`
	Included Included        `json:"included"`
	Links    *Links          `json:"links"`
	Meta     json.RawMessage `json:"meta,omitempty"`
}

func (r *ConditionListResponse) PutMeta(v interface{}) (err error) {
	r.Meta, err = json.Marshal(v)
	return err
}

func (r *ConditionListResponse) GetMeta(out interface{}) error {
	return json.Unmarshal(r.Meta, out)
}

// MustCondition - returns Condition from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustCondition(key Key) *Condition {
	var condition Condition
	if c.tryFindEntry(key, &condition) {
		return &condition
	}
	return nil
}
