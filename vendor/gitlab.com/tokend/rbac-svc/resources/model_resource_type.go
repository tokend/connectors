/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	CONTITIONS ResourceType = "contitions"
	POLICIES   ResourceType = "policies"
)
