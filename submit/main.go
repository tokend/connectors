package submit

import (
	connector "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/json-api-connector/client"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/lazyinfo"
	"gitlab.com/tokend/go/xdrbuild"
	"net/url"
	"strings"
)

type Submitter struct {
	base          *connector.Connector
	info          *lazyinfo.LazyInfoer
	submissionUrl *url.URL
}

func New(client client.Client) *Submitter {
	submission, _ := url.Parse("/v3/transactions")

	return &Submitter{
		base:          connector.NewConnector(client),
		info:          lazyinfo.New(client),
		submissionUrl: submission,
	}
}

// WithSubmissionPath sets custom submission path instead of default horizon's `/v3/transactions`.
// This is useful for building modules that do submit transactions to another modules that have compatible interface.
//
// E.g. in case we mint tokens from Go code using NFT module with horizon-compatible Mint interface
// on endpoint /integrations/nft/mint, this method should be used as follows:
//
//     baseURL, _ := url.Parse("https://api.whatever.tokend.io")
//     client := signed.NewRawClient(http.DefaultClient, baseURL)
//
//     submitter := submit.New(client)
//
//     submissionURL, _ := url.Parse("/integrations/nft/mint")
//     submitter := submitter.WithSubmissionPath(submissionURL)
//
//     result, err := submitter.Submit(context.Background(), envelope, true, false)
//

func (t *Submitter) WithSubmissionPath(path *url.URL) *Submitter {
	if path.Scheme != "" || path.Host != "" || strings.HasPrefix(path.Path, "/") {
		panic("path must be relative and start with `/` (e.g. `/v3/transactions`)")
	}

	return &Submitter{
		base:          t.base,
		info:          t.info,
		submissionUrl: path,
	}
}

func (t *Submitter) TXBuilder() (*xdrbuild.Builder, error) {
	info, err := t.info.Info()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get horizon info")
	}

	return xdrbuild.NewBuilder(info.Attributes.NetworkPassphrase, info.Attributes.TxExpirationPeriod), nil
}
