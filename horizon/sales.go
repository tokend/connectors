package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const salesURL = "/v3/sales"

func (c *Connector) Sales(query ...Query) ([]regources.Sale, *regources.Included, error) {
	var response regources.SaleListResponse

	err := c.GetList(salesURL, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.Sale{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting sales")
	}

	return response.Data, &response.Included, nil
}

func (c *Connector) SaleByID(id string, query ...Query) (*regources.Sale, *regources.Included, error) {
	var response regources.SaleResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: salesURL,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting sale")
	}

	return &response.Data, &response.Included, nil
}
