package horizon

import (
	"encoding/json"
	"net/url"
	"reflect"

	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/json-api-connector/client"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/keyvalue"
	"gitlab.com/tokend/connectors/lazyinfo"
	"gitlab.com/tokend/connectors/request"
	"gitlab.com/tokend/connectors/submit"
	"gitlab.com/tokend/keypair"
	regources "gitlab.com/tokend/regources/generated"
)

type Connector struct {
	*horizon.Connector
	*submit.Submitter
	*lazyinfo.LazyInfoer
	*keyvalue.KeyValuer
	*request.Reviewer
	Client client.Client
	Signer keypair.Full
}

func NewConnector(cli client.Client, signer keypair.Full, waitResult, waitForIngest bool) *Connector {
	return &Connector{
		Client:     cli,
		Submitter:  submit.New(cli),
		Connector:  horizon.NewConnector(cli),
		KeyValuer:  keyvalue.New(cli),
		LazyInfoer: lazyinfo.New(cli),
		Reviewer: request.New(
			cli,
			keyer.Keys{
				Signer: signer,
				Source: signer,
			},
			waitResult, waitForIngest,
		),
		Signer: signer,
	}
}

func (c *Connector) GetList(url string, response interface{}, query ...Query) error {
	streamer := horizon.NewStreamer(c.Client, url, Queries(query))

	rv := reflect.ValueOf(response)
	rt := rv.Elem().Type()
	if rt.Kind() == reflect.Ptr {
		rt = rt.Elem()
	}
	newResponse := reflect.New(rt).Interface()

	var dataFieldName, includedFieldName string
	for i := 0; i < rt.NumField(); i++ {
		if v, ok := rt.Field(i).Tag.Lookup("json"); ok {
			switch v {
			case "data":
				dataFieldName = rt.Field(i).Name
			case "included":
				includedFieldName = rt.Field(i).Name
			}
		}
	}

	var mergedIncluded []json.RawMessage

	for {
		temp := reflect.New(rt).Interface()
		if err := streamer.Next(temp); err != nil {
			return errors.Wrap(err, "failed to get data list")
		}

		tempDataField := reflect.ValueOf(temp).Elem().FieldByName(dataFieldName)
		if !tempDataField.IsValid() {
			return errors.New("no Data field")
		}

		if tempDataField.IsNil() || tempDataField.Len() == 0 {
			break
		}

		tempIncludedField := reflect.ValueOf(temp).Elem().FieldByName(includedFieldName)
		if !tempIncludedField.IsValid() {
			return errors.New("no Included field")
		}

		newDataField := reflect.ValueOf(newResponse).Elem().FieldByName(dataFieldName)
		if !newDataField.IsValid() {
			return errors.New("response does not have a Data field")
		}
		newDataField.Set(reflect.AppendSlice(newDataField, tempDataField))

		if tempIncludedBytes, err := tempIncludedField.Interface().(regources.Included).MarshalJSON(); err == nil {
			var tempIncludedObject []json.RawMessage
			if err := json.Unmarshal(tempIncludedBytes, &tempIncludedObject); err != nil {
				return errors.Wrap(err, "failed to unmarshal included")
			}

			mergedIncluded = append(mergedIncluded, tempIncludedObject...)

			continue
		}

		return errors.New("failed to marshal included")
	}

	newIncludedField := reflect.ValueOf(newResponse).Elem().FieldByName(includedFieldName)
	if !newIncludedField.IsValid() {
		return errors.New("response does not have an Included field")
	}
	mergedIncludedBytes, err := json.Marshal(mergedIncluded)
	if err != nil {
		return errors.Wrap(err, "failed to marshal merged included")
	}
	if err := newIncludedField.Addr().Interface().(json.Unmarshaler).UnmarshalJSON(mergedIncludedBytes); err != nil {
		return errors.Wrap(err, "failed to unmarshal included")
	}

	toSet := reflect.ValueOf(newResponse)

	if rv.Elem().Kind() != reflect.Ptr {
		toSet = toSet.Elem()
	}

	rv.Elem().Set(toSet)
	return nil
}

func (c *Connector) GetOne(p Path, response interface{}, query ...Query) error {
	path, err := url.Parse(p.Path())
	if err != nil {
		return errors.Wrap(err, "failed to parse url")
	}

	values := make(url.Values)

	for _, q := range query {
		values.Add(q.Field, q.StringValue())
	}

	path.RawQuery = values.Encode()

	err = c.Get(path, response)
	if err != nil {
		if err == horizon.ErrNotFound {
			return nil
		}
		return err
	}

	return nil
}
