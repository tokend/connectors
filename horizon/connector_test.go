package horizon

import (
	"net/http"
	"net/url"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tokend/keypair"

	"gitlab.com/tokend/connectors/signed"
)

// Default is http://localhost:8000/_/api
var envApiUrl = os.Getenv("TEST_URL")

// Default is SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4
var envSigner = os.Getenv("TEST_ADMIN")

var testApiUrl, _ = url.Parse(envApiUrl)
var testSigner, _ = keypair.ParseSeed(envSigner)

var testConfig = Config{
	Endpoint: testApiUrl,
	Signer:   testSigner,
	Timeout:  10000,
}

func getConnector(t *testing.T) *Connector {
	assert.NotEmpty(t, testConfig.Signer)
	assert.NotEmpty(t, testConfig.Endpoint)
	assert.NotEmpty(t, testConfig.Timeout)

	cli := signed.NewClient(
		&http.Client{
			Timeout: time.Duration(testConfig.Timeout) * time.Millisecond,
		},
		testConfig.Endpoint,
	).WithSigner(testConfig.Signer)

	return NewConnector(cli, testConfig.Signer, true, false)
}

func TestAssetUSD(t *testing.T) {
	connector := getConnector(t)
	asset, _, err := connector.AssetByID("USD")

	assert.NoError(t, err)
	assert.NotNil(t, asset)
}

func TestAssetsOwnerAdmin(t *testing.T) {
	connector := getConnector(t)
	assets, _, err := connector.Assets(GetFilterOwner(connector.Signer.Address()))

	assert.NoError(t, err)
	assert.NotEmpty(t, assets)
}

func TestBalancesUSD(t *testing.T) {
	connector := getConnector(t)
	balances, _, err := connector.Balances(GetFilterAsset("USD"))

	assert.NoError(t, err)
	assert.NotEmpty(t, balances)
}

func TestBalanceAdminUSD(t *testing.T) {
	connector := getConnector(t)
	balances, _, err := connector.Balances(GetFilterAsset("USD"), GetFilterOwner(connector.Signer.Address()))

	assert.NoError(t, err)
	assert.Len(t, balances, 1)
}

func TestAccountAdmin(t *testing.T) {
	connector := getConnector(t)
	account, _, err := connector.AccountById(connector.Signer.Address())

	assert.NoError(t, err)
	assert.NotNil(t, account)
}

func TestAccountsAdmin(t *testing.T) {
	connector := getConnector(t)
	account, _, err := connector.Accounts(GetFilterAccount(connector.Signer.Address()))

	assert.NoError(t, err)
	assert.NotNil(t, account)
}

func TestAssetPairsBaseAssetBTC(t *testing.T) {
	connector := getConnector(t)
	pairs, _, err := connector.AssetPairs(GetFilterBaseAsset("BTC"))

	assert.NoError(t, err)
	assert.NotEmpty(t, pairs)
}

func TestAssetPairsQuoteAssetUSD(t *testing.T) {
	connector := getConnector(t)
	pairs, _, err := connector.AssetPairs(GetFilterQuoteAsset("USD"))

	assert.NoError(t, err)
	assert.NotEmpty(t, pairs)
}

func TestAssetPairBTCUSD(t *testing.T) {
	connector := getConnector(t)
	pair, _, err := connector.AssetPairByID("BTC:USD")

	assert.NoError(t, err)
	assert.NotNil(t, pair)
}
