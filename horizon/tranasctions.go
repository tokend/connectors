package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const transactionsUrl = "/v3/transactions"

func (c *Connector) Transactions(query ...Query) ([]regources.Transaction, *regources.Included, error) {
	var response regources.TransactionListResponse

	err := c.GetList(transactionsUrl, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.Transaction{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting transactions")
	}

	return response.Data, &response.Included, err
}

func (c *Connector) TransactionById(id string, query ...Query) (*regources.Transaction, *regources.Included, error) {
	var response regources.TransactionResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: transactionsUrl,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting transaction")
	}

	return &response.Data, &response.Included, err
}
