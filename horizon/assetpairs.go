package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const assetPairsUrl = "/v3/asset_pairs"

func (c *Connector) AssetPairs(query ...Query) ([]regources.AssetPair, *regources.Included, error) {
	var response regources.AssetPairListResponse

	err := c.GetList(assetPairsUrl, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.AssetPair{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting asset pairs")
	}

	return response.Data, &response.Included, nil
}

func (c *Connector) AssetPairByID(id string, query ...Query) (*regources.AssetPair, *regources.Included, error) {
	var response regources.AssetPairResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: assetPairsUrl,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting asset pair")
	}

	return &response.Data, &response.Included, nil
}
