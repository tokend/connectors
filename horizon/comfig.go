package horizon

import (
	"net/http"
	"net/url"
	"time"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"

	"gitlab.com/tokend/connectors/signed"
)

const ConnectorDefaultTimeout = 30 * time.Second

type Config struct {
	Endpoint      *url.URL      `fig:"endpoint,required"`
	Signer        keypair.Full  `fig:"signer,required"`
	Timeout       time.Duration `fig:"timeout"`
	WaitResult    bool          `fig:"wait_result"`
	WaitForIngest bool          `fig:"wait_for_result"`
}

type Connectorer interface {
	Connector() *Connector
}

type connectorer struct {
	getter kv.Getter
	once   comfig.Once
}

func NewConnectorer(getter kv.Getter) Connectorer {
	return &connectorer{
		getter: getter,
	}
}

func (c *connectorer) Connector() *Connector {
	var config = Config{
		Timeout:    ConnectorDefaultTimeout,
		WaitResult: true,
	}

	return c.once.Do(func() interface{} {
		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "connector")).
			Please()

		if err != nil {
			panic(err)
		}

		if config.Signer == nil {
			panic("configure signer!")
		}

		cli := signed.NewClient(
			&http.Client{
				Timeout: config.Timeout,
			},
			config.Endpoint,
		).WithSigner(config.Signer)
		return NewConnector(cli, config.Signer, config.WaitResult, config.WaitForIngest)
	}).(*Connector)
}
